<?php

class RecipeTest extends \PHPUnit_Framework_TestCase
{
	private $fridge;
	private $recipes;

	public function __construct()
	{
		$csvfile = dirname(__FILE__).DS.'fridge.csv';
		$this->fridge = new \Models\Fridge($csvfile);

		$jsonfile = dirname(__FILE__).DS.'recipes.json';
		$this->recipes = new \Models\Recipe($jsonfile);
	}

	public function testFridgeContentsCount()
	{
		$contentCount = $this->fridge->getContentsCount();
		$this->assertEquals(5, $contentCount);
	}

	public function testExpiredIngredient()
	{
		$content = $this->fridge->getExpiredIngredient();
		$this->assertEquals(1, count($content));
	}

	public function testGoodIngredient()
	{
		$content = $this->fridge->getGoodIngredient();

		$this->assertEquals(4, count($content));
	}

	public function testTodayTime()
	{
		$time = $this->fridge->getTodayTime();

		$this->assertEquals(strtotime(date('d-m-Y')), $time);
	}

	public function testRecipesCount()
	{
		$recipesCount = $this->recipes->getRecipesCount();
		$this->assertEquals(2, $recipesCount);
	}

	public function testMatchContentAndRecipe()
	{
		$goodContent = $this->fridge->getGoodIngredient();
		$recipes = $this->recipes->getMatchStockRecipe($goodContent);

		$this->assertEquals(2, count($recipes));
	}

	public function testRecommendedRecipe()
	{
		$goodContent = $this->fridge->getGoodIngredient();
		$matchesRecipes = $this->recipes->getMatchStockRecipe($goodContent);
		$recipe = $this->recipes->getRecommendedRecipe($matchesRecipes);
		$expectedRecipe = array(
			"name"        => "salad sandwich",
			"ingredients" => array(
				array(
					"item"   => "bread",
					"amount" => "2",
					"unit"   => "slices"
				),
				array(
					"item"   => "mixed salad",
					"amount" => "200",
					"unit"   => "grams"
				)
			),
			"closestUseby" => 1412085600
		);

		$this->assertEquals($expectedRecipe, $recipe);
	}

	public function testRecommendedRecipeFromStocks()
	{
		$goodContent = $this->fridge->getGoodIngredient();
		$recipe = $this->recipes->getRecommendedRecipeFromStocks($goodContent);
		$expectedRecipe = array(
			"name"        => "salad sandwich",
			"ingredients" => array(
				array(
					"item"   => "bread",
					"amount" => "2",
					"unit"   => "slices"
				),
				array(
					"item"   => "mixed salad",
					"amount" => "200",
					"unit"   => "grams"
				)
			),
			"closestUseby" => 1412085600
		);

		$this->assertEquals($expectedRecipe, $recipe);
	}
}