<?php

namespace Controllers;

class IndexController extends \Libraries\Controller
{
	public function indexAction()
	{
		$this->view();
	}
}