<?php
define('DS', DIRECTORY_SEPARATOR);
define('BASEPATH', realpath(dirname(__FILE__)));
define('VIEWPATH', realpath(dirname(__FILE__).DS.'Views'));

date_default_timezone_set("Australia/Sydney");

function __autoload($class)
{
	$parts = explode('\\', $class);
	$classFile = implode(DS, $parts) . '.php';
	if (is_file($classFile)) {
		require_once $classFile;
	}
	// return FALSE;
}

spl_autoload_register('__autoload');