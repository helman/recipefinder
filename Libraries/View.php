<?php

namespace Libraries;

class View{

	private $_params = array();

	protected $_DI;
	protected $_dispatcher;

	function __construct()
	{
		$this->_DI = \Libraries\DI::getInstance();
	}

	function setParam($paramName, $paramValue)
	{
		$this->_params[$paramName] = $paramValue;
	}

	function get($serviceName)
	{
		return DI::get($serviceName);
	}

	function baseUrl($path = null)
	{
		return $this->get('url')->getBaseurl($path);
	}

	function render($template = null)
	{
		if (!$this->_dispatcher) {
			$this->_dispatcher = $this->get('dispatcher');
		}

		$template = $template ? $template : $this->_dispatcher->getAction();

		$viewFile = VIEWPATH.DS.$this->_dispatcher->getController().DS.$template.'.php';
		if (is_file($viewFile)) {
			header('Content-Type: text/html; charset=utf-8');
			extract($this->_params);
			include $viewFile;
		} else {
			throw new \Exception("Undefine view file '{$viewFile}' ", 1);

		}
	}
}