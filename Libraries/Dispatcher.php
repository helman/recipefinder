<?php

namespace Libraries;

class Dispatcher{

	protected $_DI;
	protected $_path;
	protected $_controller;
	protected $_action;

	private $_baseUrl;
	private $_currentUrl;

	public function __construct()
	{
		$this->_DI = \Libraries\DI::getInstance();
	}

	private function _parseUrl()
	{
		$controller = 'index';
		$action = 'index';

		$paths = explode('/', $this->_path);

		// get controller name
		if (isset($paths[0]) && !empty($paths[0])) {
			$controller = $paths[0];
			array_splice($paths, 0, 1);
		}

		// get action name
		if (isset($paths[0]) && !empty($paths[0])) {
			$action = $paths[0];
			array_splice($paths, 0, 1);
		}

		return array($controller, $action, $paths);
	}

	public function getController()
	{
		return $this->_controller;
	}

	public function getAction()
	{
		return $this->_action;
	}

	public function setPath($path)
	{
		$this->_path = ltrim($path, '/');
	}

	public function setController($controller)
	{
		$this->_controller = $controller;
	}

	public function setAction($action)
	{
		$this->_action = $action;
	}

	public function dispatch()
	{
		list($controller, $action, $args) = $this->_parseUrl();

		$controllerClass = "\\Controllers\\".ucwords($controller)."Controller";
		$actionFunction = strtolower($action).'Action';

		if (class_exists($controllerClass)) {
			$this->setController($controller);
			$class = new $controllerClass;
			if (method_exists($class, $actionFunction)) {
				$this->setAction($action);
				$class->$actionFunction($args);
			} else {
				throw new \Exception("Undefined action '{$action}'", 1);

			}
		} else {
			throw new \Exception("Undefined controller '{$controller}'", 1);
		}
	}

}